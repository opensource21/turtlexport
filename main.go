package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	conf := parseArgs()
	jsonFile, err := os.Open(conf.backupJson)
	handleError(err, fmt.Sprintf("Can't open %s.", conf.backupJson))
	log.Println("Successfully Opened users.json")
	// defer the closing of our jsonFile so that we can parse it later on
	//goland:noinspection GoUnhandledErrorResult
	defer jsonFile.Close()
	// read our opened jsonFile as a byte array.
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var turtlExport TurtlExport
	err = json.Unmarshal(byteValue, &turtlExport)
	handleError(err, "Can't unmarshal "+conf.backupJson)

	// Map id to name
	boards := make(map[string]string, 0)
	spaces := make(map[string]string, 0)

	_ = os.RemoveAll(conf.outoutPath)
	createDirectory(conf.outoutPath)
	for _, space := range turtlExport.Spaces {
		spaces[space.ID] = space.Title
		directory := filepath.Join(conf.outoutPath, space.Title)
		createDirectory(directory)
	}
	for _, board := range turtlExport.Boards {
		boards[board.ID] = board.Title
		directory := filepath.Join(conf.outoutPath, spaces[board.SpaceID], board.Title)
		createDirectory(directory)
	}
	for _, note := range turtlExport.Notes {
		filename := filepath.Join(conf.outoutPath, spaces[note.SpaceID], boards[note.BoardID], strings.Trim(note.Title, " ")+".md")
		content := note.Text
		createMarkdownFile(filename, content, note.Tags)
	}

	if len(turtlExport.Files) > 0 {
		log.Printf("Warning %d files are ignored.\n", len(turtlExport.Files))
	}
}

func createMarkdownFile(filename string, content string, tags []string) {
	header := ""
	if len(tags) > 0 {
		header = "---\ntags: [" + strings.Join(tags, ", ") + "]\n---\n"
	}

	file, err := os.Create(filename)
	handleError(err, fmt.Sprintf("Can't create %s", filename))
	//goland:noinspection GoUnhandledErrorResult
	defer file.Close()
	_, err = file.WriteString(header + content + "\n")
	handleError(err, fmt.Sprintf("Can't write test to %s", filename))
	log.Printf("Create file %s.\n", filename)
}

func createDirectory(directory string) {
	err := os.Mkdir(directory, 0700)
	handleError(err, "Can't create directory "+directory)
}

func parseArgs() conf {
	conf := conf{}
	flag.StringVar(&conf.backupJson, "backupJson", "", "Path to backup-file.")
	flag.StringVar(&conf.outoutPath, "outoutPath", "", "Path to where the files are stored.")
	flag.Parse()
	log.Printf("Loaded configuration: %#v", conf)
	return conf
}

func handleError(err error, message string) {
	if err != nil {
		log.Panicf("ERROR: %s: %v", message, err)
	}
}

type conf struct {
	backupJson string
	outoutPath string
}

type TurtlExport struct {
	Boards []struct {
		Body    interface{}   `json:"body"`
		ID      string        `json:"id"`
		Keys    []interface{} `json:"keys"`
		SpaceID string        `json:"space_id"`
		Title   string        `json:"title"`
		UserID  int           `json:"user_id"`
	} `json:"boards"`
	Files []struct {
		Body interface{} `json:"body"`
		Data string      `json:"data"`
		ID   string      `json:"id"`
	} `json:"files"`
	Notes []struct {
		BoardID string `json:"board_id"`
		Body    string `json:"body"`
		File    struct {
			Body string `json:"body"`
		} `json:"file"`
		HasFile bool   `json:"has_file"`
		ID      string `json:"id"`
		Keys    []struct {
			K string `json:"k"`
			S string `json:"s,omitempty"`
			B string `json:"b,omitempty"`
		} `json:"keys"`
		Mod     int      `json:"mod"`
		SpaceID string   `json:"space_id"`
		Tags    []string `json:"tags"`
		Text    string   `json:"text"`
		Title   string   `json:"title"`
		Type    string   `json:"type"`
		UserID  int      `json:"user_id"`
	} `json:"notes"`
	SchemaVersion int `json:"schema_version"`
	Spaces        []struct {
		Body    interface{}   `json:"body"`
		Color   string        `json:"color"`
		ID      string        `json:"id"`
		Invites []interface{} `json:"invites"`
		Keys    []interface{} `json:"keys"`
		Members []interface{} `json:"members"`
		Title   string        `json:"title"`
		UserID  int           `json:"user_id"`
	} `json:"spaces"`
}
